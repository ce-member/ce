module.exports = {
  servers: {
    one: {
      // TODO: set host address, username, and authentication method
      host: '178.128.100.224',
      username: 'root',
      // pem: './path/to/pem'
      password: 'e925b27243e4dcf9cac9d6d225aAa'
      // or neither for authenticate from ssh-agent
    }
  },

  app: {
    // TODO: change app name and path
    name: 'CE',
    path: 'C:/Users/sutee/Desktop/ce',

    servers: {
      one: {},
    },

    buildOptions: {
      serverOnly: true,
    },

    env: {
      // TODO: Change to your app's url
      // If you are using ssl, it needs to start with https://
      PORT: 5600,
      ROOT_URL: 'http://178.128.100.224',
      MONGO_URL: 'mongodb://poom:F2rw3kdP67GflGigtaVCP8mzaGZlTmkofAK6KLLE@128.199.214.155:27017/poom',
    },

    docker: {
      // change to 'abernix/meteord:base' if your app is using Meteor 1.4 - 1.5
      image: 'abernix/meteord:node-8.4.0-base',
    },

    // Show progress bar while uploading bundle to server
    // You might need to disable it on CI servers
    enableUploadProgressBar: true
  },



  // (Optional)
  // Use the proxy to setup ssl or to route requests to the correct
  // app when there are several apps

  // proxy: {
  //   domains: 'mywebsite.com,www.mywebsite.com',

  //   ssl: {
  //     // Enable Let's Encrypt
  //     letsEncryptEmail: 'email@domain.com'
  //   }
  // }
};
