import mysql from 'mysql'

const mysqlconfig = {
    host: Meteor.settings.public.sqlhost,
    user: Meteor.settings.public.sqluser,
    password: Meteor.settings.public.sqlpassword,
    database: Meteor.settings.public.sqldatabase,
    insecureAuth: true
}
//console.log(mysqlconfig)
const queryfn = (res, query) => {
    return new Promise(function (resolve, reject) {
        const con = mysql.createConnection(mysqlconfig)
        con.connect(function (err) {
            if (err) {
                console.log(err)
            } else {
                con.query(query, function (err, result, fields) {

                    if (err) {
                        // console.log(err)
                        console.log({ "errorCode": err.code })
                        res.send({ "errorCode": err.code })
                    } else {
                        resolve(result)

                        res.send(result)

                    }
                    con.end();
                })
            }

        })
    })
}



export default queryfn;

