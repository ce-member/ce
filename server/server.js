import {
  Meteor
} from 'meteor/meteor';
// import {
//   SERVICE_CENTER
// } from '../imports/db.js';
// import {
//   DISTRICT
// } from '../imports/db.js';
import queryfn from "./app/query";
// import get_execel from "./app/excel";
// import get_execelhos from "./app/excelhos";
import bodyParser from "body-parser";
import cors from "cors";
import express from "express";

const server = express();
const CryptoJS = require("crypto-js");

server.use(
  cors(),
  bodyParser.json(),
  bodyParser.urlencoded({
      extended: true
  })
);
const formatDate = date => moment(date).format("YYYY-MM-DD HH:mm:ss")
const onlyDate = date => moment(date).format("YYYY-MM-DD")
WebApp.connectHandlers.use(Meteor.bindEnvironment(server));

Meteor.startup(() => {
  // code to run on server at startup
});
server.post("/insert-result", (req, res) => {

  const {
      body
  } = req;
const query = `UPDATE savemom.CE 
SET CID = '${body.CID}', HOSNAME = '${body.HOSNAME}', SUBDISTRICT = '${body.SUBDISTRICT}', DISTRICT = '${body.DISTRICT}', PROVINCE = '${body.PROVINCE}', REPORTDATE = '${body.REPORTDATE}', NAME = '${body.NAME}', LNAME = '${body.LNAME}', AGE = '${body.AGE}', NATIONALITY = '${body.NATIONALITY}', NATIONALITY_OTHER = '${body.NATIONALITY_OTHER}', CAREER = '${body.CAREER}', OUTNUMBER = '${body.OUTNUMBER}', INNUMBER = '${body.INNUMBER}', DATE_DEAD = '${body.DATE_DEAD}',GES_AGE_MOM_DEAD = '${body.GES_AGE_MOM_DEAD}', ANTENATAL_CARE = '${body.ANTENATAL_CARE}', NO_PREGNANCY = '${body.NO_PREGNANCY}',WIEGHT_ANTENATAL_CARE = '${body.WIEGHT_ANTENATAL_CARE}', HIGHT = '${body.HIGHT}', PRE_WEIGHT = '${body.PRE_WEIGHT}', HB_HCT1 = '${body.HB_HCT1}', HB_HCT2 = '${body.HB_HCT2}', ANTIHIV = '${body.ANTIHIV}', SETBIRTH = '${body.SETBIRTH}', WOMB_RISK = '${body.WOMB_RISK}', WOMB_CH1 = '${body.WOMB_CH1}', WOMB_CH2 = '${body.WOMB_CH2}', WOMB_CH3 = '${body.WOMB_CH3}', WOMB_CH4 = '${body.WOMB_CH4}', WOMB_CH5 = '${body.WOMB_CH5}', WOMB_CH6 = '${body.WOMB_CH6}', WOMB_CH7 = '${body.WOMB_CH7}', REMAKEWOMB_CH7 = '${body.REMAKEWOMB_CH7}', HISTORY_DRUG = '${body.HISTORY_DRUG}', CASEREFER_DATE = '${body.CASEREFER_DATE}', CASEREFER_TIME = '${body.CASEREFER_TIME}', STATUS_COME_SERVICE_1 = '${body.STATUS_COME_SERVICE_1}', STATUS_COME_SERVICE_2 = '${body.STATUS_COME_SERVICE_2}', NO_BIRTH = '${body.NO_BIRTH}', NO_MISCARRIAGE = '${body.NO_MISCARRIAGE}',STATUS = '${body.STATUS}',DATE_ANTENATAL_CARE = '${body.DATE_ANTENATAL_CARE}',NO_ANTENATAL_CARE = '${body.NO_ANTENATAL_CARE}',STATUS_COME_SERVICE_3 = '${body.STATUS_COME_SERVICE_3}', STATUS_COME_SERVICE_4 = '${body.STATUS_COME_SERVICE_4}', STATUS_COME_SERVICE_5 = '${body.STATUS_COME_SERVICE_5}', STATUS_COME_SERVICE_6 = '${body.STATUS_COME_SERVICE_6}', STATUS_PATIENT_AT_SERVICE_1 = '${body.STATUS_PATIENT_AT_SERVICE_1}', STATUS_PATIENT_AT_SERVICE_2 = '${body.STATUS_PATIENT_AT_SERVICE_2}', STATUS_PATIENT_AT_SERVICE_3 = '${body.STATUS_PATIENT_AT_SERVICE_3}', STATUS_PATIENT_AT_SERVICE_4 = '${body.STATUS_PATIENT_AT_SERVICE_4}', STATUS_PATIENT_AT_SERVICE_OTHER = '${body.STATUS_PATIENT_AT_SERVICE_OTHER}', CASEREFER = '${body.CASEREFER}', PARTOGRAPH = '${body.PARTOGRAPH}', TERM_1 = '${body.TERM_1}', TERM_1_1 = '${body.TERM_1_1}', TERM_2 = '${body.TERM_2}', TERM_2_1 = '${body.TERM_2_1}', TERM_3 = '${body.TERM_3}', TERM_3_1 = '${body.TERM_3_1}', TIME_AMNIOTIC = '${body.TIME_AMNIOTIC}', COLOR_AMNIOTIC = '${body.COLOR_AMNIOTIC}', DATE_BIRTH = '${body.DATE_BIRTH}', TIME_BIRTH = '${body.TIME_BIRTH}', MISCARRIAGE = '${body.MISCARRIAGE}', BIRTH = '${body.BIRTH}', BIRTH_OTHER = '${body.BIRTH_OTHER}', ACCOUCHEUR = '${body.ACCOUCHEUR}', ACCOUCHEUR_OTHER = '${body.ACCOUCHEUR_OTHER}', BIRTH_WEIGHT = '${body.BIRTH_WEIGHT}', BIRTH_DESCRIPTION = '${body.BIRTH_DESCRIPTION}', APGAR_SCORE_1 = '${body.APGAR_SCORE_1}', APGAR_SCORE_5 = '${body.APGAR_SCORE_5}', BIRTH_DESCRIPTION_WEIGHT = '${body.BIRTH_DESCRIPTION_WEIGHT}', BEHAVIOUR_MOM = '${body.BEHAVIOUR_MOM}', CHOICE1 = '${body.CHOICE1}', CHOICE2 = '${body.CHOICE2}', CHOICE3 = '${body.CHOICE3}', CHOICE4 = '${body.CHOICE4}', CHOICE5 = '${body.CHOICE5}', CHOICE6 = '${body.CHOICE6}', CHOICE7 = '${body.CHOICE7}', CHOICE8 = '${body.CHOICE8}', CHOICE9 = '${body.CHOICE9}', CH_1 = '${body.CH_1}', SUB_CH_1 = '${body.SUB_CH_1}', CH_1_2 = '${body.CH_1_2}', SUB_CH_1_2 = '${body.SUB_CH_1_2}', CH_2 = '${body.CH_2}', SUB_CH_2 = '${body.SUB_CH_2}', CH_2_2 = '${body.CH_2_2}', SUB_CH_2_2 = '${body.SUB_CH_2_2}', CH_2_3 = '${body.CH_2_3}', SUB_CH_2_3 = '${body.SUB_CH_2_3}', CH_3 = '${body.CH_3}', SUB_CH_3 = '${body.SUB_CH_3}', CH_3_2 = '${body.CH_3_2}', SUB_CH_3_2 = '${body.SUB_CH_3_2}', CH_3_3 = '${body.CH_3_3}', SUB_CH_3_3 = '${body.SUB_CH_3_3}', CH_3_4 = '${body.CH_3_4}', SUB_CH_3_4 = '${body.SUB_CH_3_4}', AUTOSPY = '${body.AUTOSPY}', REMARKAUTOSPY = '${body.REMARKAUTOSPY}', EVENT_DEAD = '${body.EVENT_DEAD}', PROTECT_DEAD = '${body.PROTECT_DEAD}', SUGGESTION = '${body.SUGGESTION}', VERIFY_NAME = '${body.VERIFY_NAME}', VERIFY_LNAME = '${body.VERIFY_LNAME}', VERIFY_DATE = '${body.VERIFY_DATE}', REPORT_NAME = '${body.REPORT_NAME}', REPORT_LNAME = '${body.REPORT_LNAME}', REPORT_DATE = '${body.REPORT_DATE}', CAREER_OTHER = '${body.CAREER_OTHER}',  VDRL = '${body.VDRL}', PRE_WEIGHT_ANTENATAL_CARE = '${body.PRE_WEIGHT_ANTENATAL_CARE}', HIGHT_2 = '${body.HIGHT_2}', HIGHT_3 = '${body.HIGHT_3}', BMI1 = '${body.BMI1}', BMI2 = '${body.BMI2}', BMI3 = '${body.BMI3}', OB_DEPARTMENT = '${body.OB_DEPARTMENT}', POSITION = '${body.POSITION}', SEND_DEPARTMENT = '${body.SEND_DEPARTMENT}'
WHERE ID = '${body.ID}';
`;
  console.log(query)
  queryfn(res, query);
});

// server.post("/get-domain", (req, res) => {
//   const {
//       body
//   } = req;
//   const query = 'SELECT DOMAIN FROM savemom.HOSPITAL GROUP BY DOMAIN;';
//   queryfn(res, query);
// });
server.post("/get-province", (req, res) => {
  const {
      body
  } = req;
  const query = 'SELECT PROVINCE FROM savemom.HOSPITAL GROUP BY PROVINCE;';
  queryfn(res, query);
});
server.post("/get-district", (req, res) => {
  const {
      body
  } = req;
  const query = 'SELECT DISTRICT FROM savemom.HOSPITAL WHERE PROVINCE="' + body.province + '" GROUP BY  DISTRICT;';
  queryfn(res, query);
});
server.post("/get-subdistrict", (req, res) => {
  const {
      body
  } = req;
  const query = 'SELECT SUBDISTRICT FROM savemom.HOSPITAL WHERE DISTRICT="' + body.district + '" and PROVINCE ="' + body.province + '" GROUP BY SUBDISTRICT;';
  console.log(query)
  queryfn(res, query);
});server.post("/get-hospital", (req, res) => {
  const {
      body
  } = req;
  const query = 'SELECT  HOSPCODE,NAME FROM savemom.HOSPITAL WHERE DISTRICT="' + body.district + '" and PROVINCE ="' + body.province + '" and SUBDISTRICT= "' + body.subdistrict + '" GROUP BY NAME,HOSPCODE;';
  queryfn(res, query);
});







server.post("/get-result-year", (req, res) => {
  const {
      body
  } = req;
  const query = `SELECT ID,CID,NAME,LNAME,DATE_DEAD FROM savemom.CE  
  where DATE_DEAD between'${body.yearsearch-1}-10-01'and '${body.yearsearch}-09-30';`;
  queryfn(res, query);
});
server.post("/get-result-domain", (req, res) => {
  const {
      body
  } = req;
  const query = `SELECT ID,CID,NAME,LNAME,DATE_DEAD FROM savemom.CE  
  where DATE_DEAD between'${body.yearsearch-1}-10-01'and '${body.yearsearch}-09-30' and DOMAIN='${body.DOMAIN}';`;
  queryfn(res, query);
});

server.post("/get-ce", (req, res) => {
  const {
      body
  } = req;
  const query = 'SELECT * FROM savemom.CE';
  queryfn(res, query);
});