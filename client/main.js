const OS = require('jquery');
myfuc = (function () {
    var api = {};

    function getDateToday() {
        var today = moment(new Date()).format("DD/MM/YYYY");
        var split_today = today.split('/')
        var date_today = split_today[0] + "/" + split_today[1] + "/" + (parseInt(split_today[2]) + 543)
        return date_today
    }

    api.showpic = function(data, pic) {
        // console.log(data);
        $('.' + data).hide();
        $("body").removeClass("grey lighten-5");
        $("body").addClass("black");
        $(".showload").append('<script>$("#close").click(function(){myfuc.onclosepic("' + data + '")});</script><a id="close" style="color:white"> <br><center> กดที่รูปเพื่อปิด <br><br><img src=' + pic + ' style="width:80%;max-height:500px;max-width:500px"> <br><br></center> </a>');
        $(".showload").fadeIn();
    }

    api.onclosepic = function(data) {
        // console.log(data);
        $('.' + data).show();
        $("body").addClass("grey lighten-5");
        $("body").removeClass("white");
        $(".showload").empty();
    }

    api.datepickerth = function () {
        var months = [
            'มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
            'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
        ];
        var monthsShort = [
            'ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.',
            'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'
        ];
        var weekdays = [
            'อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'
        ];
        var weekdaysAbv = [
            'อ.', 'จ', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.'
        ];
        // เริ่มตรวจับอินพุต datepicker ใช้งาน Materialize.css datepickers
        // เก็บ datepicker ทั้งหมดไว้ที่ตัวแปร 
        let datepickers = OS('.datepicker');

        let instances = [];

        // เลือกแต่ละ input ของ datepicker มาใส่ฟังก์ชั่นเพิ่มเติ่ม
        for (let element of datepickers) {
            // เลือก Materialize Datepicker มาใช้
            let datepicker = $(element).datepicker({
                container: document.body,
                i18n: {
                    months: months,
                    monthsShort: monthsShort,
                    weekdays: weekdays,
                    weekdaysShort: weekdays,
                    weekdaysAbbrev: weekdaysAbv,
                    cancel: 'ยกเลิก',
                    clear: 'ล้างข้อมูล',
                    done: 'ตกลง'
                },
                defaultDate: Date.now(),
                setDefaultDate: true,
                format: 'dd/mm/yyyy',
                yearRange: [1950, 2020],
                onSelect: function (d) {
                    // onSelect event จะใช้ฟังก์ชั่น onUse ด้วย
                    if (typeof this.onUse == 'function') this.onUse('select', d);
                },
                onOpen: function () {
                    // onOpen event จะใช้ฟังก์ชั่น onUse ด้วย
                    if (typeof this.onUse == 'function') this.onUse('open');
                },
                onClose: function () {
                    // if (typeof this.onUse == 'function') this.onUse('close');
                },
                onDraw: function () {
                    // กรณีที่มีการเปลี่ยนแปลงของตัว datepicker แต่ยังไม่ได้เยืนยันเลือกวันที่จริงๆ
                    // จะใช้ onDraw ของ Materialize เตรียมให้
                    let modalSelection = OS('.dropdown-content.select-dropdown>li>span');
                    let that = this;

                    // เนื่องจาก onDraw event ทำงาน จะยังไม่สามารถแก้ไขค่าที่แสดงบน datepicker ทันที
                    // จึงต้องมีการใส่ delay เพื่อรอให้ Materialize แสดงผลสมบูรณ์ก่อน
                    // การแสดงผลปีที่เปลี่ยนไปจะพบว่าขาดความลื่นไหลต่อเนื่อง อาจเห็นการเปลี่ยนแปลงที่ไม่เนียนสวยงาม
                    let render = function () {
                        setTimeout(function () {
                            let yearInput = OS('.datepicker-modal').find('.select-wrapper.select-year>.select-dropdown.dropdown-trigger');
                            let sideYearText = OS('.datepicker-modal').find('.year-text');
                            let year = datepicker.calendars[0].year + 543;
                            yearInput.val(year);
                            sideYearText.text(year);
                        }, 0);
                    }

                    // ใส่ onclick event ของ dropdown ที่ใช้เลือกปีหรือเดือนบนปฏิทิน
                    // แก้ไขปีที่แสดงจาก ค.ศ. เป็นปี พ.ศ.
                    for (let list of Array.from(modalSelection)) {
                        let isNotYear = isNaN(parseInt(OS(list).text()));
                        if (!isNotYear) {
                            OS(list).text(parseInt(OS(list).text()) + 543);
                        }
                        OS(list).on('click', function () {
                            render();
                        });
                    }

                    // ใส่ onclick event ของปุ่มเลื่อนเดือนบนปฏิทิน
                    // อัพเดทปีที่แสดงจาก ค.ศ. เป็นปี พ.ศ.
                    OS('.datepicker-modal')
                        .find('.month-next, .month-prev')
                        .on('click', function () {
                            render();
                        });
                },

            }).get(0).M_Datepicker;

            // onUse จะเป็นฟังก์ชั่นที่ใช้เปลี่ยนค่าปี ค.ศ. เป็นปี พ.ศ.
            // Event ของ Materialize เช่น onSelect และ onOpen จะใช้ฟังก์ชั่นนี้ด้วย
            datepicker.onUse = function (evName, date) {
                let that = this;
                let render = function () {
                    let displayYear = that.yearTextEl;
                    let inputYear = OS(that.calendarEl).find('.select-wrapper.select-year>.select-dropdown.dropdown-trigger');

                    inputYear.val(parseInt(inputYear.val()) + 543);
                    displayYear.innerText = parseInt(inputYear.val());

                };

                if (evName == 'open') {
                    setTimeout(render, 0);
                } else {
                    render();
                }
            };

            // เมื่อมีการเลือกวันที่แล้ว onchange event ของ input จะทำงาน
            // เปลี่ยนปี ค.ศ. ให้เป็น ปี พ.ศ.
            OS(datepicker.el).on('change', function () {
                let date = OS(this).val().split(/\//);
                date[2] = parseInt(date[2]) + 543 + '';
                OS(this).val(date.join('/'));
            })


            instances.push(element.M_Datepicker);
        }
        window.DatepickerInstances = instances;
    }
    api.datepickerth_auto = function () {
        var months = [
            'มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
            'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
        ];
        var monthsShort = [
            'ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.',
            'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'
        ];
        var weekdays = [
            'อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'
        ];
        var weekdaysAbv = [
            'อ.', 'จ', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.'
        ];
        // เริ่มตรวจับอินพุต datepicker ใช้งาน Materialize.css datepickers
        // เก็บ datepicker ทั้งหมดไว้ที่ตัวแปร 
        let datepickers = OS('.datepicker');

        let instances = [];

        // เลือกแต่ละ input ของ datepicker มาใส่ฟังก์ชั่นเพิ่มเติ่ม
        for (let element of datepickers) {
            // เลือก Materialize Datepicker มาใช้

            let datepicker = $(element).datepicker({
                container: document.body,
                i18n: {
                    months: months,
                    monthsShort: monthsShort,
                    weekdays: weekdays,
                    weekdaysShort: weekdays,
                    weekdaysAbbrev: weekdaysAbv,
                    cancel: 'ยกเลิก',
                    clear: 'ล้างข้อมูล',
                    done: 'ตกลง'
                },
                // defaultDate: Date.now(),
                setDefaultDate: false,
                format: 'dd/mm/yyyy',
                yearRange: [1950, 2019],
                onSelect: function (d) {
                    // onSelect event จะใช้ฟังก์ชั่น onUse ด้วย
                    if (typeof this.onUse == 'function') this.onUse('select', d);
                },
                onOpen: function () {
                    // onOpen event จะใช้ฟังก์ชั่น onUse ด้วย
                    if (typeof this.onUse == 'function') this.onUse('open');
                },
                onClose: function () {
                    // if (typeof this.onUse == 'function') this.onUse('close');
                },
                onDraw: function () {
                    // กรณีที่มีการเปลี่ยนแปลงของตัว datepicker แต่ยังไม่ได้เยืนยันเลือกวันที่จริงๆ
                    // จะใช้ onDraw ของ Materialize เตรียมให้
                    let modalSelection = OS('.dropdown-content.select-dropdown>li>span');
                    let that = this;

                    // เนื่องจาก onDraw event ทำงาน จะยังไม่สามารถแก้ไขค่าที่แสดงบน datepicker ทันที
                    // จึงต้องมีการใส่ delay เพื่อรอให้ Materialize แสดงผลสมบูรณ์ก่อน
                    // การแสดงผลปีที่เปลี่ยนไปจะพบว่าขาดความลื่นไหลต่อเนื่อง อาจเห็นการเปลี่ยนแปลงที่ไม่เนียนสวยงาม
                    let render = function () {
                        setTimeout(function () {

                            let yearInput = OS('.datepicker-modal').find('.select-wrapper.select-year>.select-dropdown.dropdown-trigger');
                            let sideYearText = OS('.datepicker-modal').find('.year-text');
                            let year = datepicker.calendars[0].year + 543;
                            yearInput.val(year);
                            sideYearText.text(year);
                        }, 0);
                    }

                    // ใส่ onclick event ของ dropdown ที่ใช้เลือกปีหรือเดือนบนปฏิทิน
                    // แก้ไขปีที่แสดงจาก ค.ศ. เป็นปี พ.ศ.
                    for (let list of Array.from(modalSelection)) {
                        let isNotYear = isNaN(parseInt(OS(list).text()));
                        if (!isNotYear) {

                            OS(list).text(parseInt(OS(list).text()) + 543);
                        }
                        OS(list).on('click', function () {
                            render();
                        });
                    }

                    // ใส่ onclick event ของปุ่มเลื่อนเดือนบนปฏิทิน
                    // อัพเดทปีที่แสดงจาก ค.ศ. เป็นปี พ.ศ.
                    OS('.datepicker-modal')
                        .find('.month-next, .month-prev')
                        .on('click', function () {
                            render();
                        });
                },

            }).get(0).M_Datepicker;

            // onUse จะเป็นฟังก์ชั่นที่ใช้เปลี่ยนค่าปี ค.ศ. เป็นปี พ.ศ.
            // Event ของ Materialize เช่น onSelect และ onOpen จะใช้ฟังก์ชั่นนี้ด้วย
            //console.log(datepicker)
            datepicker.onUse = function (evName, date) {
                let that = this;
                let render = function () {
                    let displayYear = that.yearTextEl;
                    let inputYear = OS(that.calendarEl).find('.select-wrapper.select-year>.select-dropdown.dropdown-trigger');

                    inputYear.val(parseInt(inputYear.val()) + 543);

                    displayYear.innerText = parseInt(inputYear.val());

                };

                if (evName == 'open') {
                    setTimeout(render, 0);
                } else {
                    render();
                }
            };

            // เมื่อมีการเลือกวันที่แล้ว onchange event ของ input จะทำงาน
            // เปลี่ยนปี ค.ศ. ให้เป็น ปี พ.ศ.

            OS(datepicker.el).on('change', function () {
                let date = OS(this).val().split(/\//);
                if(date[2] >= 1800 && date[2] <= 2200){

                    date[2] = parseInt(date[2]) + 543 + '';
                    OS(this).val(date.join('/'));

                }
  
            })

            instances.push(element.M_Datepicker);
        }
        $('.datepicker').val(getDateToday())
        window.DatepickerInstances = instances;

    }


       api.ndataTable = function($, DataTable) {
          "use strict";
      
          $('.search-toggle').click(function() {
            if ($('.hiddensearch').css('display') == 'none')
              $('.hiddensearch').slideDown();
            else
              $('.hiddensearch').slideUp();
          });
      
          /* Set the defaults for DataTables initialisation */
          $.extend(true, DataTable.defaults, {
            dom: "<'hiddensearch'f'>" +
              "tr" +
              "<'table-footer'lip'>",
            renderer: 'material'
          });
      
          /* Default class modification */
          $.extend(DataTable.ext.classes, {
            sWrapper: "dataTables_wrapper",
            sFilterInput: "form-control input-sm",
            sLengthSelect: "form-control input-sm"
          });
      
          /* Bootstrap paging button renderer */
          DataTable.ext.renderer.pageButton.material = function(settings, host, idx, buttons, page, pages) {
            var api = new DataTable.Api(settings);
            var classes = settings.oClasses;
            var lang = settings.oLanguage.oPaginate;
            var btnDisplay, btnClass, counter = 0;
      
            var attach = function(container, buttons) {
              var i, ien, node, button;
              var clickHandler = function(e) {
                e.preventDefault();
                if (!$(e.currentTarget).hasClass('disabled')) {
                  api.page(e.data.action).draw(false);
                }
              };
      
              for (i = 0, ien = buttons.length; i < ien; i++) {
                button = buttons[i];
      
                if ($.isArray(button)) {
                  attach(container, button);
                } else {
                  btnDisplay = '';
                  btnClass = '';
      
                  switch (button) {
      
                    case 'first':
                      btnDisplay = lang.sFirst;
                      btnClass = button + (page > 0 ?
                        '' : ' disabled');
                      break;
      
                    case 'previous':
                      btnDisplay = '<i class="material-icons">chevron_left</i>';
                      btnClass = button + (page > 0 ?
                        '' : ' disabled');
                      break;
      
                    case 'next':
                      btnDisplay = '<i class="material-icons">chevron_right</i>';
                      btnClass = button + (page < pages - 1 ?
                        '' : ' disabled');
                      break;
      
                    case 'last':
                      btnDisplay = lang.sLast;
                      btnClass = button + (page < pages - 1 ?
                        '' : ' disabled');
                      break;
      
                  }
      
                  if (btnDisplay) {
                    node = $('<li>', {
                        'class': classes.sPageButton + ' ' + btnClass,
                        'id': idx === 0 && typeof button === 'string' ?
                          settings.sTableId + '_' + button : null
                      })
                      .append($('<a>', {
                          'href': '#',
                          'aria-controls': settings.sTableId,
                          'data-dt-idx': counter,
                          'tabindex': settings.iTabIndex
                        })
                        .html(btnDisplay)
                      )
                      .appendTo(container);
      
                    settings.oApi._fnBindAction(
                      node, {
                        action: button
                      }, clickHandler
                    );
      
                    counter++;
                  }
                }
              }
            };
      
            // IE9 throws an 'unknown error' if document.activeElement is used
            // inside an iframe or frame. 
            var activeEl;
      
            try {
              // Because this approach is destroying and recreating the paging
              // elements, focus is lost on the select button which is bad for
              // accessibility. So we want to restore focus once the draw has
              // completed
              activeEl = $(document.activeElement).data('dt-idx');
            } catch (e) {}
      
            attach(
              $(host).empty().html('<ul class="material-pagination"/>').children('ul'),
              buttons
            );
      
            if (activeEl) {
              $(host).find('[data-dt-idx=' + activeEl + ']').focus();
            }
          };
      
          /*
           * TableTools Bootstrap compatibility
           * Required TableTools 2.1+
           */
          if (DataTable.TableTools) {
            // Set the classes that TableTools uses to something suitable for Bootstrap
            $.extend(true, DataTable.TableTools.classes, {
              "container": "DTTT btn-group",
              "buttons": {
                "normal": "btn btn-default",
                "disabled": "disabled"
              },
              "collection": {
                "container": "DTTT_dropdown dropdown-menu",
                "buttons": {
                  "normal": "",
                  "disabled": "disabled"
                }
              },
              "print": {
                "info": "DTTT_print_info"
              },
              "select": {
                "row": "active"
              }
            });
      
            // Have the collection use a material compatible drop down
            $.extend(true, DataTable.TableTools.DEFAULTS.oTags, {
              "collection": {
                "container": "ul",
                "button": "li",
                "liner": "a"
              }
            });
          }
      
        }; // /factory
      
      

    return api;


}());
