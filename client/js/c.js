import {
  Template
} from 'meteor/templating'
const M = require('materialize-css');
const axios = require('axios')

function ResetPageObj() {
  var pageObj = [{
      page: 1,
      div: "#page_1",

    },
    {
      page: 2,
      div: "#page_2",
      form: ["", ""],
      result: ["", ""]
    },
    {
      page: 3,
      div: "#page_3",
      form: [""],
      result: [""]
    },
    {
      page: 4,
      div: "#page_4",
      form: [""],
      result: [""]
    },
    {
      page: 5,
      div: "#page_5",
      form: [""],
      result: [""]
    },
    {
      page: 6,
      div: "#page_6",
      form: [""],
      result: [""]
    },
    {
      page: 7,
      div: "#page_7",
      form: [""],
      result: [""]
    },
    {
      page: 8,
      div: "#page_8",
      form: [""],
      result: [""]
    },
    {
      page: 9,
      div: "#page_9",
      form: [""],
      result: [""]
    },
    {
      page: 10,
      div: "#page_10",
      form: [""],
      result: [""]
    },
  ]
  return pageObj
}

function checkedtruefalse(c) {
  if (c) {
    return '1'
  } else {
    return '0'
  }
}

function checkeundefined(u) {
  if (u === undefined) {
    return ''
  } else {
    return u
  }
}

function checkenull(n) {
  if (n == null) {
    return ''
  } else {
    return n
  }
}

function fomatDate(x) {
  if (x == '') {
    return x = ''
  } else {
    var data = x.split('/')
    return (data[2] - 543) + "-" + data[1] + "-" + data[0]
  }
}

function fomatDatetothai(x) {
  if (x == '') {
    return x = ''
  } else {
    var data = x.split('/')
    return (parseInt(data[2]) + 543) + "-" + data[1] + "-" + data[0]
  }
}
Template.c.onRendered(function () {
  // Session.set('id',null)
  Session.set('number', null)
  $('.modal').modal();
  ninetynine();
  Session.set('_page', 0)
  pageObj = ResetPageObj();
  Session.set('province', null)
  Session.set('district', null)
  Session.set('subdistrict', null)
  Session.set('hospital', null)
  Session.set('MCH_province', null)
  Session.set('MCH_district', null)
  Session.set('MCH_subdistrict', null)
  Session.set('MCH_hospital', null)

  getprovince();

  $('.timepicker').timepicker({
    twelveHour: false,
  });
  $('#set_button').css('display', 'block')
  var _page = Session.get('_page')
  _page += 1
  Session.set('_page', _page)
  for (i = 0; i < pageObj.length; i++) {
    if (_page == pageObj[i].page) {
      $(pageObj[i].div).fadeIn(400)
    } else {
      $(pageObj[i].div).hide()
    }
  }
  if (_page == 1) {
    $('#back-btn').addClass("disabled")
    $('#next-btn').removeClass("disabled")
  } else if (_page == 10) {
    $('#next-btn').addClass("disabled")
  } else if (_page > 1) {
    $('#back-btn').removeClass("disabled")
    $('#next-btn').removeClass("disabled")
  }
  myfuc.datepickerth_auto()
});

Template.c.helpers({
  showPage() {
    return Session.get('_page')
  },
  province() {
    return Session.get('province')
  },
  district() {
    return Session.get('district')
  },
  subdistrict() {
    return Session.get('subdistrict')
  },
  hospital() {
    return Session.get('hospital')
  },
  MCH_province() {
    return Session.get('MCH_province')
  },
  MCH_district() {
    return Session.get('MCH_district')
  },
  MCH_subdistrict() {
    return Session.get('MCH_subdistrict')
  },
  MCH_hospital() {
    return Session.get('MCH_hospital')
  },
  number() {
    console.log(Session.get('number'))
    return Session.get('number')
  },


});

Template.c.events({

  'keyup #HIGHT': function (event, template) {
    if ($('#HIGHT').val() > 0 && $('#WIEGHT_ANTENATAL_CARE').val() > 0) {
      console.log($('#HIGHT').val())
      let r = calculateBmi($('#WIEGHT_ANTENATAL_CARE').val(), $('#HIGHT').val());
      console.log(r)
    }
  },
  'keyup #WIEGHT_ANTENATAL_CARE': function (event, template) {

    if ($('#HIGHT').val() > 0 && $('#WIEGHT_ANTENATAL_CARE').val() > 0) {
      let r = calculateBmi($('#WIEGHT_ANTENATAL_CARE').val(), $('#HIGHT').val());
      $("#bmi").html(r);

    }
  },
  "click #history-go"() {
    $('#set_button').css('display', 'block')
    var _page = Session.get('_page')
    _page += 1
    Session.set('_page', _page)
    for (i = 0; i < pageObj.length; i++) {
      if (_page == pageObj[i].page) {
        $(pageObj[i].div).fadeIn(400)
      } else {
        $(pageObj[i].div).hide()
      }
    }
    if (_page == 1) {
      $('#back-btn').addClass("disabled")
      $('#next-btn').removeClass("disabled")
    } else if (_page == 10) {
      $('#next-btn').addClass("disabled")
    } else if (_page > 1) {
      $('#back-btn').removeClass("disabled")
      $('#next-btn').removeClass("disabled")
    }
  },
  "click #next-btn"() {
    // $("html, body").animate({ scrollTop: 0 }, "fast");
    $('html,body').scrollTop(0);
    var _page = Session.get('_page')
    _page += 1
    Session.set('_page', _page)
    for (i = 0; i < pageObj.length; i++) {
      if (_page == pageObj[i].page) {
        $(pageObj[i].div).fadeIn(400)
      } else {
        $(pageObj[i].div).hide()
      }
    }
    if (_page == 1) {
      $('#back-btn').addClass("disabled")

    } else if (_page == 10) {
      $('#next-btn').addClass("disabled")

    } else if (_page > 1) {
      $('#back-btn').removeClass("disabled")
      $('#next-btn').removeClass("disabled")
    }
  },
  "click #back-btn"() {
    var _page = Session.get('_page')
    _page -= 1
    Session.set('_page', _page)
    for (i = 0; i < pageObj.length; i++) {
      if (_page == pageObj[i].page) {
        $(pageObj[i].div).fadeIn(400)
      } else {
        $(pageObj[i].div).hide()
      }
    }
    if (_page == 1) {
      $('#back-btn').addClass("disabled")
    } else if (_page == 10) {
      $('#next-btn').addClass("disabled")
    } else if (_page > 1) {
      $('#back-btn').removeClass("disabled")
      $('#next-btn').removeClass("disabled")
    }
  },
  "change input[name=ANTENATAL_CARE]:checked"() {

    if ($('input[name=ANTENATAL_CARE]:checked').val() == '1') {
      $('#div-ANTENATAL_CARE').show()
    } else {
      $('#div-ANTENATAL_CARE').hide()
    }
  },
  "change input[name=WOMB_RISK]:checked"() {

    if ($('input[name=WOMB_RISK]:checked').val() == '1') {
      $('#div-WOMB_RISK').show()
    } else {
      $('#div-WOMB_RISK').hide()
    }
  },
  "change input[name=AUTOSPY]:checked"() {

    if ($('input[name=AUTOSPY]:checked').val() == '1') {
      $('#div-AUTOSPY').show()
    } else {
      $('#div-AUTOSPY').hide()
    }
  },
  "change input[name=PARTOGRAPH]:checked"() {

    if ($('input[name=PARTOGRAPH]:checked').val() == '1') {
      $('#div-PARTOGRAPH').show()
    } else {
      $('#div-PARTOGRAPH').hide()
    }
  },
  'change #PROVINCE'() {
    getdistrict_by_province($('#PROVINCE').val());
  },
  'change #DISTRICT'() {
    getsubdistrict_by_district($('#DISTRICT').val(), $('#PROVINCE').val());
  },
  'change #SUBDISTRICT'() {
    gethospictal_by_subdistrict($('#SUBDISTRICT').val(), $('#DISTRICT').val(), $('#PROVINCE').val());
  },
  'change #MCH_PROVINCE'() {
    MCH_getdistrict_by_province($('#MCH_PROVINCE').val());
  },
  'change #MCH_DISTRICT'() {
    MCH_getsubdistrict_by_district($('#MCH_DISTRICT').val(), $('#MCH_PROVINCE').val());
  },
  'change #MCH_SUBDISTRICT'() {
    MCH_gethospictal_by_subdistrict($('#MCH_SUBDISTRICT').val(), $('#MCH_DISTRICT').val(), $('#MCH_PROVINCE').val());
  },
  'change input[name=NATIONALITY]:checked'() {
    if ($('input[name=NATIONALITY]:checked').val() == '4') {
      $('#div-NATIONALITY_OTHER').show()

    } else {
      $('#div-NATIONALITY_OTHER').hide()
      $('#NATIONALITY_OTHER').val('')
    }
  },
  'change #WOMB_CH7'() {
    if ($("#WOMB_CH7").is(':checked')) {
      $('#div-REMAKEWOMB_CH7').show()
      $("#WOMB_CH7").is(':checked')
    } else {
      $('#div-REMAKEWOMB_CH7').hide()
      $('#REMAKEWOMB_CH7').val('')
    }
  },
  'change #STATUS_PATIENT_AT_SERVICE_4'() {
    if ($("#STATUS_PATIENT_AT_SERVICE_4").is(':checked')) {
      $('#div-STATUS_PATIENT_AT_SERVICE_OTHER').show()
      $("#STATUS_PATIENT_AT_SERVICE_4").is(':checked')
    } else {
      $('#div-STATUS_PATIENT_AT_SERVICE_OTHER').hide()
      $('#STATUS_PATIENT_AT_SERVICE_OTHER').val('')
    }
  },
  'change input[name=ACCOUCHEUR]:checked'() {
    if ($('input[name=ACCOUCHEUR]:checked').val() == '3') {
      $('#div-ACCOUCHEUR_OTHER').show()

    } else {
      $('#div-ACCOUCHEUR_OTHER').hide()
      $('#ACCOUCHEUR_OTHER').val('')
    }
  },
  "click #save-btn"() {
    $('#modal1').modal('open');
    var date = fomatDatetothai(moment().format('DD/MM/YYYY'))
    date = moment(date).format('YY')
    axios.post(`${Meteor.settings.public.api_host}/count-all`, {}).then(function (response) {
        Session.set('id', parseInt(date) * 10000 + response.data[0].all + 1)
      })
      .catch(function (error) {
        console.log(error)
      })


  },
  "click #modal-save"() {
    axios.post(`${Meteor.settings.public.api_host}/insert-easy-form`, {

        ID: Session.get('id'),
        HOSNAME: checkenull($('#HOSNAME').val()),
        SUBDISTRICT: checkenull($('#SUBDISTRICT').val()),
        DISTRICT: checkenull($('#DISTRICT').val()),
        PROVINCE: checkenull($('#PROVINCE').val()),
        REPORTDATE: fomatDate($('#REPORTDATE').val()),

        NAME: $('#NAME').val(),
        LNAME: $('#LNAME').val(),
        AGE: $('#AGE').val(),
        NO_PREGNANCY: checkenull($('#NO_PREGNANCY').val()),
        DATE_DEAD: fomatDate($('#DATE_DEAD').val()),

        GES_AGE_MOM_DEAD: $('#GES_AGE_MOM_DEAD').val(),
        PRIMARY_CAUSE_OF_DEATH: $('#PRIMARY_CAUSE_OF_DEATH').val(),
        CASEREFER: checkeundefined($('input[name=CASEREFER]:checked').val()),

      }).then(function (response) {
        // console.log(response)
      })
      .catch(function (error) {
        console.log(error)
      })
    Router.go('result')
    // M.toast({
    //   "html": 'บันทึกข้อมูลเรียบร้อย'
    // })

  },
  "click #modal-go"() {
    axios.post(`${Meteor.settings.public.api_host}/insert-easy-form`, {

        ID: Session.get('id'),
        HOSNAME: checkenull($('#HOSNAME').val()),
        SUBDISTRICT: checkenull($('#SUBDISTRICT').val()),
        DISTRICT: checkenull($('#DISTRICT').val()),
        PROVINCE: checkenull($('#PROVINCE').val()),
        REPORTDATE: fomatDate($('#REPORTDATE').val()),

        NAME: $('#NAME').val(),
        LNAME: $('#LNAME').val(),
        AGE: $('#AGE').val(),
        NO_PREGNANCY: checkenull($('#NO_PREGNANCY').val()),
        DATE_DEAD: fomatDate($('#DATE_DEAD').val()),

        GES_AGE_MOM_DEAD: $('#GES_AGE_MOM_DEAD').val(),
        PRIMARY_CAUSE_OF_DEATH: $('#PRIMARY_CAUSE_OF_DEATH').val(),
        CASEREFER: checkeundefined($('input[name=CASEREFER]:checked').val()),

      }).then(function (response) {
   
        Router.go('ce')
      })
      .catch(function (error) {
        console.log(error)
      })
  }
})

function getprovince() {
  axios
    .post(`${Meteor.settings.public.api_host}/get-province`)
    .then(function (response) {
      console.log(response.data)
      Meteor.setTimeout(() => {
        $('select').formSelect()
      }, 500)
      Session.set('province', response.data)
    })
    .catch(function (error) {
      console.log(error)
    })
}

function getdistrict_by_province(province) {
  axios
    .post(`${Meteor.settings.public.api_host}/get-district`, {
      province: province
    })
    .then(function (response) {
      console.log(response.data)
      Meteor.setTimeout(() => {
        $('select').formSelect()
      }, 500)
      Session.set('district', response.data)
    })
    .catch(function (error) {
      console.log(error)
    })
}

function getsubdistrict_by_district(district, province) {
  axios
    .post(`${Meteor.settings.public.api_host}/get-subdistrict`, {
      district: district,
      province: province
    })
    .then(function (response) {
      console.log(response.data)
      Meteor.setTimeout(() => {
        $('select').formSelect()
      }, 500)
      Session.set('subdistrict', response.data)
    })
    .catch(function (error) {
      console.log(error)
    })
}

function gethospictal_by_subdistrict(subdistrict, district, province) {
  axios
    .post(`${Meteor.settings.public.api_host}/get-hospital`, {
      subdistrict: subdistrict,
      district: district,
      province: province
    })
    .then(function (response) {
      console.log(response.data)
      Meteor.setTimeout(() => {
        $('select').formSelect()
      }, 500)
      Session.set('hospital', response.data)
    })
    .catch(function (error) {
      console.log(error)
    })
}



function ninetynine() {
  let arr = []
  for (let index = 1; index < 100; index++) {

    arr.push({
        index: index
      }

    )

  }
  Session.set('number', arr)
}