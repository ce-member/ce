import {
    Template
} from 'meteor/templating'
const M = require('materialize-css');
const axios = require('axios')


Template.result.onRendered(function () {
    $('select').formSelect();
    // domain();
    allprovince();
    search_data_year($('#yearsearch').val());
});

Template.result.helpers({
    subdistrict() {
        return Session.get('subdistrict')
    },
    district() {
        return Session.get('district')
    },
    province() {
        return Session.get('province')
    },
    domain() {
        return Session.get('domain')
    },
    allyear() {
        return Session.get('allyear')
    },
    alldata() {
        return Session.get('alldata')
    }
});

Template.result.events({
    'change #yearsearch'() {
        search_data_year($('#yearsearch').val())
        $('#provincesearch option[value=' + 'all' + ']').prop('selected', true)
        $('#districtsearch option[value=' + 'all' + ']').prop('selected', true)
        $('#subdistrictsearch option[value=' + 'all' + ']').prop('selected', true)
        $('#district-div').hide()
        $('#subdistrict-div').hide()
    },
    // 'change #domainsearch'() {
    //     // alert($('#domainsearch').val())

    // },
    'change #provincesearch'() {
        // alert($('#provincesearch').val())
        provincesearch($('#provincesearch').val());
        //
        if ($('#provincesearch').val() == 'all') {
            search_data_year($('#yearsearch').val())
            $('#districtsearch option[value=' + 'all' + ']').prop('selected', true)
            $('#subdistrictsearch option[value=' + 'all' + ']').prop('selected', true)
            $('#district-div').hide()
            $('#subdistrict-div').hide()
        } else {
            search_data_province($('#yearsearch').val(), $('#provincesearch').val());
            $('#districtsearch option[value=' + 'all' + ']').prop('selected', true)
            $('#subdistrictsearch option[value=' + 'all' + ']').prop('selected', true)
            $('#district-div').show()
            $('#subdistrict-div').hide()
        }
    },
    'change #districtsearch'() {
        // alert($('#districtsearch').val())
        districtsearch($('#provincesearch').val(), $('#districtsearch').val());
        //
        if ($('#districtsearch').val() == 'all') {
            search_data_province($('#yearsearch').val(), $('#provincesearch').val());
            $('#subdistrictsearch option[value=' + 'all' + ']').prop('selected', true)
            $('#subdistrict-div').hide()
        } else {
            search_data_district($('#yearsearch').val(), $('#provincesearch').val(), $('#districtsearch').val());
            $('#subdistrictsearch option[value=' + 'all' + ']').prop('selected', true)
            $('#subdistrict-div').show()
        }
    },
    'change #subdistrictsearch'() {
        // alert($('#subdistrictsearch').val())

        ///
        if ($('#subdistrictsearch').val() == 'all') {
            search_data_district($('#yearsearch').val(), $('#provincesearch').val(), $('#districtsearch').val());
        } else {
            search_data_subdistrict($('#yearsearch').val(), $('#provincesearch').val(), $('#districtsearch').val(), $('#subdistrictsearch').val());
        }
    },
    'change .checkeasy'() {
        Session.set('id', this.ID)
        Router.go('c_view')
    },
    'change .checkfull'() {
        Session.set('id', this.ID)
        Router.go('ce_view')
    },
})

Template.registerHelper("SELECTDOMAIN13", function (data) {
    if (data == "13") {
        return "กทม"
    } else {
        return 'เขต ' + data;
    }
});
Template.registerHelper("SELECTPROVINCE13", function (data) {
    if (data == "กรุงเทพมหานคร") {
        return data
    } else {
        return 'จ.' + data;
    }
});
Template.registerHelper("SELECTDISTRICT13", function (data) {
    var splitstrings = data.split('ต')
    if (splitstrings[0] == "เข") {
        return data
    } else {
        return 'อ.' + data;
    }
});
Template.registerHelper("SELECTSUBDISTRICT13", function (data) {

});

function get_year() {
    axios
        .post(`${Meteor.settings.public.api_host}/get-risk-minyear`)
        .then(function (response) {
            //  console.log(new Date())
            var year = []
            for (
                var i = new Date().getFullYear() + 1; i >= response.data[0].minyear - 2; i--
            ) {
                year.push({
                    year: i + 543
                })
            }
            Session.set('allyear', year)
            setTimeout(() => {
                let year = new Date().getFullYear() + 543;
                $('#yearsearch option[value=' + year + ']').prop(
                    'selected',
                    true
                )
                $('select').formSelect()
            }, 500);
            setTimeout(() => {
                search_all_domain()
                getdomainmaker()
            }, 1000)
        })
        .catch(function (error) {
            console.log(error)
        })
}

function allprovince() {
    axios
        .post(`${Meteor.settings.public.api_host}/get-province`, {

        })
        .then(function (response) {
            Meteor.setTimeout(() => {
                $('select').formSelect()
            }, 500)
            Session.set('province', response.data)
        })
        .catch(function (error) {
            console.log(error)
        })
}
// function domain() {
//     axios
//         .post(`${Meteor.settings.public.api_host}/get-domain`)
//         .then(function (response) {
//             Meteor.setTimeout(() => {
//                 $('select').formSelect()
//             }, 500)
//             Session.set('domain', response.data)
//         })
//         .catch(function (error) {
//             console.log(error)
//         })
// }

// function domainsearch(domain) {
//     axios
//         .post(`${Meteor.settings.public.api_host}/get-province`, {
//             domain: domain
//         })
//         .then(function (response) {
//             Meteor.setTimeout(() => {
//                 $('select').formSelect()
//             }, 500)
//             Session.set('province', response.data)
//         })
//         .catch(function (error) {
//             console.log(error)
//         })
// }

function provincesearch(province) {
    axios
        .post(`${Meteor.settings.public.api_host}/get-district`, {
            province: province,

        })
        .then(function (response) {
            Meteor.setTimeout(() => {
                $('select').formSelect()
            }, 500)
            Session.set('district', response.data)
        })
        .catch(function (error) {
            console.log(error)
        })
}

function districtsearch(province, district) {
    axios
        .post(`${Meteor.settings.public.api_host}/get-subdistrict`, {
            province: province,
            district: district
        })
        .then(function (response) {
            console.log(response.data)
            Meteor.setTimeout(() => {
                $('select').formSelect()
            }, 500)
            Session.set('subdistrict', response.data)
        })
        .catch(function (error) {
            console.log(error)
        })
}
////scerch
function search_data_year(yearsearch) {
    axios
        .post(`${Meteor.settings.public.api_host}/get-result-year`, {
            yearsearch: parseInt(yearsearch) - 543
        })
        .then(function (response) {
            Session.set('alldata', response.data)
            setTimeout(() => {
                for (let index = 0; index < response.data.length; index++) {
                    if (response.data[index].CID !=null) {
                        $('input[value=' + response.data[index].ID + ']').prop('checked', true);
                    }
                }
            }, 0);
        })
        .catch(function (error) {
            console.log(error)
        })
}

function search_data_province(yearsearch, PROVINCE) {
    axios
        .post(`${Meteor.settings.public.api_host}/get-result-province`, {
            yearsearch: parseInt(yearsearch) - 543,
            PROVINCE: PROVINCE
        })
        .then(function (response) {
            Session.set('alldata', response.data)
        })
        .catch(function (error) {
            console.log(error)
        })
}

function search_data_district(yearsearch, PROVINCE, DISTRICT) {
    axios
        .post(`${Meteor.settings.public.api_host}/get-result-district`, {
            yearsearch: parseInt(yearsearch) - 543,
            PROVINCE: PROVINCE,
            DISTRICT: DISTRICT
        })
        .then(function (response) {
            Session.set('alldata', response.data)
        })
        .catch(function (error) {
            console.log(error)
        })
}

function search_data_subdistrict(yearsearch, PROVINCE, DISTRICT, SUBDISTRICT) {
    axios
        .post(`${Meteor.settings.public.api_host}/get-result-subdistrict`, {
            yearsearch: parseInt(yearsearch) - 543,
            PROVINCE: PROVINCE,
            DISTRICT: DISTRICT,
            SUBDISTRICT: SUBDISTRICT
        })
        .then(function (response) {
            Session.set('alldata', response.data)
        })
        .catch(function (error) {
            console.log(error)
        })
}
Template.registerHelper("date_to_thai", function (data) {
    let year = parseInt(moment(data).format('YYYY')) + 543;
    let daymonth = moment(data).format('DD/MM');
    console.log(daymonth + '/' + year)
    return daymonth + '/' + year;
});