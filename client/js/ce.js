import {
  Template
} from 'meteor/templating'
const M = require('materialize-css');
const axios = require('axios')

function ResetPageObj() {
  var pageObj = [{
      page: 1,
      div: "#page_1",

    },
    {
      page: 2,
      div: "#page_2",
      form: ["", ""],
      result: ["", ""]
    },
    {
      page: 3,
      div: "#page_3",
      form: [""],
      result: [""]
    },
    {
      page: 4,
      div: "#page_4",
      form: [""],
      result: [""]
    },
    {
      page: 5,
      div: "#page_5",
      form: [""],
      result: [""]
    },
    {
      page: 6,
      div: "#page_6",
      form: [""],
      result: [""]
    },
    {
      page: 7,
      div: "#page_7",
      form: [""],
      result: [""]
    },
    {
      page: 8,
      div: "#page_8",
      form: [""],
      result: [""]
    },
    {
      page: 9,
      div: "#page_9",
      form: [""],
      result: [""]
    },
    {
      page: 10,
      div: "#page_10",
      form: [""],
      result: [""]
    },
  ]
  return pageObj
}

function checkedtruefalse(c) {
  if (c) {
    return '1'
  } else {
    return '0'
  }
}

function checkeundefined(u) {
  if (u === undefined) {
    return ''
  } else {
    return u
  }
}

function checkenull(n) {
  if (n == null) {
    return ''
  } else {
    return n
  }
}

function fomatDate(x) {
  if (x != null) {
    if (x == '') {
      return x = ''
    } else {
      var data = x.split('/')
      return (data[2] - 543) + "-" + data[1] + "-" + data[0]
    }
  }
}

Template.ce.onRendered(function () {
 
  console.log(Session.get('id'))
  Session.set('number', null)
  Session.set('_page', 0)
  pageObj = ResetPageObj();
  Session.set('province', null)
  Session.set('district', null)
  Session.set('subdistrict', null)
  Session.set('hospital', null)

  ninetynine();
  getprovince();



  $('.timepicker').timepicker({
    twelveHour: false,
    defaultTime: 'now',

  });
  $('#set_button').css('display', 'block')
  var _page = Session.get('_page')
  _page += 1
  Session.set('_page', _page)
  for (i = 0; i < pageObj.length; i++) {
    if (_page == pageObj[i].page) {
      $(pageObj[i].div).fadeIn(400)
    } else {
      $(pageObj[i].div).hide()
    }
  }
  if (_page == 1) {
    $('#back-btn').addClass("disabled")
    $('#next-btn').removeClass("disabled")
  } else if (_page == 10) {
    $('#next-btn').addClass("disabled")
  } else if (_page > 1) {
    $('#back-btn').removeClass("disabled")
    $('#next-btn').removeClass("disabled")
  }
  myfuc.datepickerth_auto()
});

Template.ce.helpers({
  showPage() {
    return Session.get('_page')
  },
  province() {
    return Session.get('province')
  },
  district() {
    return Session.get('district')
  },
  subdistrict() {
    return Session.get('subdistrict')
  },
  hospital() {
    return Session.get('hospital')
  },

  number() {
    // console.log(Session.get('number'))
    return Session.get('number')
  },
  get_id_data() {
    // console.log(Session.get('number'))
    return Session.get('get_id_data')
  },

});

Template.ce.events({

  'keyup #HIGHT_2': function (event, template) {
    if ($('#HIGHT_2').val() > 0 && $('#WIEGHT_ANTENATAL_CARE').val() > 0) {
      console.log($('#HIGHT_2').val())
      let r = calculateBmi($('#WIEGHT_ANTENATAL_CARE').val(), $('#HIGHT_2').val());
      $("#BMI2").html(r);
    }
  },
  'keyup #WIEGHT_ANTENATAL_CARE': function (event, template) {

    if ($('#HIGHT_2').val() > 0 && $('#WIEGHT_ANTENATAL_CARE').val() > 0) {
      let r = calculateBmi($('#WIEGHT_ANTENATAL_CARE').val(), $('#HIGHT_2').val());
      $("#BMI2").html(r);

    }
  },
  "click #history-go"() {
    $('#set_button').css('display', 'block')
    var _page = Session.get('_page')
    _page += 1
    Session.set('_page', _page)
    for (i = 0; i < pageObj.length; i++) {
      if (_page == pageObj[i].page) {
        $(pageObj[i].div).fadeIn(400)
      } else {
        $(pageObj[i].div).hide()
      }
    }
    if (_page == 1) {
      $('#back-btn').addClass("disabled")
      $('#next-btn').removeClass("disabled")
    } else if (_page == 10) {
      $('#next-btn').addClass("disabled")
    } else if (_page > 1) {
      $('#back-btn').removeClass("disabled")
      $('#next-btn').removeClass("disabled")
    }
  },
  "click #next-btn"() {
    // $("html, body").animate({ scrollTop: 0 }, "fast");
    $('html,body').scrollTop(0);
    var _page = Session.get('_page')
    _page += 1
    Session.set('_page', _page)
    for (i = 0; i < pageObj.length; i++) {
      if (_page == pageObj[i].page) {
        $(pageObj[i].div).fadeIn(400)
      } else {
        $(pageObj[i].div).hide()
      }
    }
    if (_page == 1) {
      $('#back-btn').addClass("disabled")

    } else if (_page == 10) {
      $('#next-btn').addClass("disabled")

    } else if (_page > 1) {
      $('#back-btn').removeClass("disabled")
      $('#next-btn').removeClass("disabled")
    }
  },
  "click #back-btn"() {
    var _page = Session.get('_page')
    _page -= 1
    Session.set('_page', _page)
    for (i = 0; i < pageObj.length; i++) {
      if (_page == pageObj[i].page) {
        $(pageObj[i].div).fadeIn(400)
      } else {
        $(pageObj[i].div).hide()
      }
    }
    if (_page == 1) {
      $('#back-btn').addClass("disabled")
    } else if (_page == 10) {
      $('#next-btn').addClass("disabled")
    } else if (_page > 1) {
      $('#back-btn').removeClass("disabled")
      $('#next-btn').removeClass("disabled")
    }
  },
  "change input[name=ANTENATAL_CARE]:checked"() {

    if ($('input[name=ANTENATAL_CARE]:checked').val() == '1') {
      $('#div-ANTENATAL_CARE').show()
    } else {
      $('#div-ANTENATAL_CARE').hide()
    }
  },
  "change input[name=WOMB_RISK]:checked"() {

    if ($('input[name=WOMB_RISK]:checked').val() == '1') {
      $('#div-WOMB_RISK').show()
    } else {
      $('#div-WOMB_RISK').hide()
    }
  },
  "change input[name=AUTOSPY]:checked"() {

    if ($('input[name=AUTOSPY]:checked').val() == '1') {
      $('#div-AUTOSPY').show()
    } else {
      $('#div-AUTOSPY').hide()
    }
  },
  "change input[name=PARTOGRAPH]:checked"() {

    if ($('input[name=PARTOGRAPH]:checked').val() == '1') {
      $('#div-PARTOGRAPH').show()
    } else {
      $('#div-PARTOGRAPH').hide()
    }
  },
  'change #PROVINCE'() {
    getdistrict_by_province($('#PROVINCE').val());
  },
  'change #DISTRICT'() {
    getsubdistrict_by_district($('#DISTRICT').val(), $('#PROVINCE').val());
  },
  'change #SUBDISTRICT'() {
    gethospictal_by_subdistrict($('#SUBDISTRICT').val(), $('#DISTRICT').val(), $('#PROVINCE').val());
  },

  'change input[name=NATIONALITY]:checked'() {
    if ($('input[name=NATIONALITY]:checked').val() == '4') {
      $('#div-NATIONALITY_OTHER').show()

    } else {
      $('#div-NATIONALITY_OTHER').hide()
      $('#NATIONALITY_OTHER').val('')
    }
  },
  'change #WOMB_CH7'() {
    if ($("#WOMB_CH7").is(':checked')) {
      $('#div-REMAKEWOMB_CH7').show()
      $("#WOMB_CH7").is(':checked')
    } else {
      $('#div-REMAKEWOMB_CH7').hide()
      $('#REMAKEWOMB_CH7').val('')
    }
  },
  'change #STATUS_PATIENT_AT_SERVICE_4'() {
    if ($("#STATUS_PATIENT_AT_SERVICE_4").is(':checked')) {
      $('#div-STATUS_PATIENT_AT_SERVICE_OTHER').show()
      $("#STATUS_PATIENT_AT_SERVICE_4").is(':checked')
    } else {
      $('#div-STATUS_PATIENT_AT_SERVICE_OTHER').hide()
      $('#STATUS_PATIENT_AT_SERVICE_OTHER').val('')
    }
  },
  'change input[name=ACCOUCHEUR]:checked'() {
    if ($('input[name=ACCOUCHEUR]:checked').val() == '3') {
      $('#div-ACCOUCHEUR_OTHER').show()

    } else {
      $('#div-ACCOUCHEUR_OTHER').hide()
      $('#ACCOUCHEUR_OTHER').val('')
    }
  },
  'change input[name=BIRTH]:checked'() {
    if ($('input[name=BIRTH]:checked').val() == '7') {
      $('#div-BIRTH_OTHER').show()

    } else {
      $('#div-BIRTH_OTHER').hide()
      $('#BIRTH_OTHER').val('')
    }
  },
  'change #CAREER'() {

    if ($('#CAREER').val() == '6') {
      $('#div-CAREER_OTHER').show()

    } else {
      $('#div-CAREER_OTHER').hide()
      $('#CAREER_OTHER').val('')
    }
  },
  "click #save-btn"() {
console.log($('#NO_ANTENATAL_CARE').val())
    axios.post(`${Meteor.settings.public.api_host}/insert-result`, {
        // ID:Session.get('id'),
        ID:Session.get('id'),
        HOSNAME: checkenull($('#HOSNAME').val()),
        SUBDISTRICT: checkenull($('#SUBDISTRICT').val()),
        DISTRICT: checkenull($('#DISTRICT').val()),
        PROVINCE: checkenull($('#PROVINCE').val()),
        REPORTDATE: fomatDate($('#REPORTDATE').val()),


        NAME: $('#NAME').val(),
        LNAME: $('#LNAME').val(),
        AGE: $('#AGE').val(),
        NATIONALITY: checkeundefined($('input[name=NATIONALITY]:checked').val()),
        NATIONALITY_OTHER: $('#NATIONALITY_OTHER').val(),
        CAREER:checkenull($('#CAREER').val()),
        CID: $('#CID').val(),
        OUTNUMBER: $('#OUTNUMBER').val(),
        INNUMBER: $('#INNUMBER').val(),
        DATE_DEAD: fomatDate($('#DATE_DEAD').val()),
        GES_AGE_MOM_DEAD: $('#GES_AGE_MOM_DEAD').val(),

        ANTENATAL_CARE: checkeundefined($('input[name=ANTENATAL_CARE]:checked').val()),
        NO_PREGNANCY: checkenull($('#NO_PREGNANCY').val()),
        NO_BIRTH: checkenull($('#NO_BIRTH').val()),
        NO_MISCARRIAGE: checkenull($('#NO_MISCARRIAGE').val()),
        DATE_ANTENATAL_CARE:checkenull($('#DATE_ANTENATAL_CARE').val()),
        NO_ANTENATAL_CARE: checkenull($('#NO_ANTENATAL_CARE').val()),
        WIEGHT_ANTENATAL_CARE: $('#WIEGHT_ANTENATAL_CARE').val(),
        HIGHT: checkenull($('#HIGHT').val()),
        PRE_WEIGHT: checkenull($('#PRE_WEIGHT').val()),
        HB_HCT1: checkenull($('#HB_HCT1').val()),
        HB_HCT2: checkenull($('#HB_HCT2').val()),
        ANTIHIV: checkenull($('#ANTIHIV').val()),
        SETBIRTH: fomatDate($('#SETBIRTH').val()),

        // ABNORMALITIES:$('#ABNORMALITIES').val(), 
        WOMB_RISK: checkeundefined($('input[name=WOMB_RISK]:checked').val()),

        WOMB_CH1: checkedtruefalse($("#WOMB_CH1").is(':checked')),
        WOMB_CH2: checkedtruefalse($("#WOMB_CH2").is(':checked')),
        WOMB_CH3: checkedtruefalse($("#WOMB_CH3").is(':checked')),
        WOMB_CH4: checkedtruefalse($("#WOMB_CH4").is(':checked')),
        WOMB_CH5: checkedtruefalse($("#WOMB_CH5").is(':checked')),
        WOMB_CH6: checkedtruefalse($("#WOMB_CH6").is(':checked')),
        WOMB_CH7: checkedtruefalse($("#WOMB_CH7").is(':checked')),
        REMAKEWOMB_CH7: $('#REMAKEWOMB_CH7').val(),
        HISTORY_DRUG: $('#HISTORY_DRUG').val(),

        CASEREFER_DATE: fomatDate($('#CASEREFER_DATE').val()),
        CASEREFER_TIME: $('#CASEREFER_TIME').val(),
        // STATUS_COME_SERVICE:checkeundefined($('input[name=STATUS_COME_SERVICE]:checked').val()),
        STATUS_COME_SERVICE_1: checkedtruefalse($("#STATUS_COME_SERVICE_1").is(':checked')),
        STATUS_COME_SERVICE_2: checkedtruefalse($("#STATUS_COME_SERVICE_2").is(':checked')),
        STATUS_COME_SERVICE_3: checkedtruefalse($("#STATUS_COME_SERVICE_3").is(':checked')),
        STATUS_COME_SERVICE_4: checkedtruefalse($("#STATUS_COME_SERVICE_4").is(':checked')),
        STATUS_COME_SERVICE_5: checkedtruefalse($("#STATUS_COME_SERVICE_5").is(':checked')),
        STATUS_COME_SERVICE_6: checkedtruefalse($("#STATUS_COME_SERVICE_6").is(':checked')),
        // STATUS_PATIENT_AT_SERVICE:checkeundefined($('input[name=STATUS_PATIENT_AT_SERVICE]:checked').val()),
        STATUS_PATIENT_AT_SERVICE_1: checkedtruefalse($("#STATUS_PATIENT_AT_SERVICE_1").is(':checked')),
        STATUS_PATIENT_AT_SERVICE_2: checkedtruefalse($("#STATUS_PATIENT_AT_SERVICE_2").is(':checked')),
        STATUS_PATIENT_AT_SERVICE_3: checkedtruefalse($("#STATUS_PATIENT_AT_SERVICE_3").is(':checked')),
        STATUS_PATIENT_AT_SERVICE_4: checkedtruefalse($("#STATUS_PATIENT_AT_SERVICE_4").is(':checked')),
        STATUS_PATIENT_AT_SERVICE_OTHER: $('#STATUS_PATIENT_AT_SERVICE_OTHER').val(),
        CASEREFER: checkeundefined($('input[name=CASEREFER]:checked').val()),

        // page6
        PARTOGRAPH: checkeundefined($('input[name=PARTOGRAPH]:checked').val()),
        TERM_1: checkenull($('#TERM_1').val()),
        TERM_1_1: checkenull($('#TERM_1_1').val()),
        TERM_2: checkenull($('#TERM_2').val()),
        TERM_2_1: checkenull($('#TERM_2_1').val()),
        TERM_3: checkenull($('#TERM_3').val()),
        TERM_3_1: checkenull($('#TERM_3_1').val()),
        TIME_AMNIOTIC: $('#TIME_AMNIOTIC').val(),
        COLOR_AMNIOTIC: checkenull($('#COLOR_AMNIOTIC').val()),
        DATE_BIRTH: fomatDate($('#DATE_BIRTH').val()),
        TIME_BIRTH: $('#TIME_BIRTH').val(),
        MISCARRIAGE: checkeundefined($('input[name=MISCARRIAGE]:checked').val()),
        BIRTH: checkeundefined($('input[name=BIRTH]:checked').val()),
        BIRTH_OTHER: $('#BIRTH_OTHER').val(),
        ACCOUCHEUR: checkeundefined($('input[name=ACCOUCHEUR]:checked').val()),
        ACCOUCHEUR_OTHER: $('#ACCOUCHEUR_OTHER').val(),
        BIRTH_WEIGHT: $('#BIRTH_WEIGHT').val(),
        BIRTH_DESCRIPTION: $('#BIRTH_DESCRIPTION').val(),
        APGAR_SCORE_1: checkenull($('#APGAR_SCORE_1').val()),
        APGAR_SCORE_5: checkenull($('#APGAR_SCORE_5').val()),
        BIRTH_DESCRIPTION_WEIGHT: $('#BIRTH_DESCRIPTION_WEIGHT').val(),
        BEHAVIOUR_MOM: $('#BEHAVIOUR_MOM').val(),

        CHOICE1: checkeundefined($('input[name=CHOICE1]:checked').val()),
        CHOICE2: checkeundefined($('input[name=CHOICE2]:checked').val()),
        CHOICE3: checkeundefined($('input[name=CHOICE3]:checked').val()),
        CHOICE4: checkeundefined($('input[name=CHOICE4]:checked').val()),
        CHOICE5: checkeundefined($('input[name=CHOICE5]:checked').val()),
        CHOICE6: checkeundefined($('input[name=CHOICE6]:checked').val()),
        CHOICE7: checkeundefined($('input[name=CHOICE7]:checked').val()),
        CHOICE8: checkeundefined($('input[name=CHOICE8]:checked').val()),
        CHOICE9: checkeundefined($('input[name=CHOICE9]:checked').val()),

        CH_1: checkeundefined($('input[name=CH_1]:checked').val()),
        SUB_CH_1: $('#SUB_CH_1').val(),
        CH_1_2: checkeundefined($('input[name=CH_1_2]:checked').val()),
        SUB_CH_1_2: $('#SUB_CH_1_2').val(),
        CH_2: checkeundefined($('input[name=CH_2]:checked').val()),
        SUB_CH_2: $('#SUB_CH_2').val(),
        CH_2_2: checkeundefined($('input[name=CH_2_2]:checked').val()),
        SUB_CH_2_2: $('#SUB_CH_2_2').val(),
        CH_2_3: checkeundefined($('input[name=CH_2_3]:checked').val()),
        SUB_CH_2_3: $('#SUB_CH_2_3').val(),
        CH_3: checkeundefined($('input[name=CH_3]:checked').val()),
        SUB_CH_3: $('#SUB_CH_3').val(),
        CH_3_2: checkeundefined($('input[name=CH_3_2]:checked').val()),
        SUB_CH_3_2: $('#SUB_CH_3_2').val(),
        CH_3_3: checkeundefined($('input[name=CH_3_3]:checked').val()),
        SUB_CH_3_3: $('#SUB_CH_3_3').val(),
        CH_3_4: checkeundefined($('input[name=CH_3_4]:checked').val()),
        SUB_CH_3_4: $('#SUB_CH_3_4').val(),

        AUTOSPY: checkeundefined($('input[name=AUTOSPY]:checked').val()),
        REMARKAUTOSPY: $('#REMARKAUTOSPY').val(),
        EVENT_DEAD: $('#EVENT_DEAD').val(),

        PROTECT_DEAD: checkeundefined($('input[name=PROTECT_DEAD]:checked').val()),
        SUGGESTION: $('#SUGGESTION').val(),


        VERIFY_NAME: $('#VERIFY_NAME').val(),
        VERIFY_LNAME: $('#VERIFY_LNAME').val(),
        VERIFY_DATE: fomatDate($('#VERIFY_DATE').val()),

        REPORT_NAME: $('#REPORT_NAME').val(),
        REPORT_LNAME: $('#REPORT_LNAME').val(),
        REPORT_DATE: fomatDate($('#REPORT_DATE').val()),

        //--new
        CAREER_OTHER: $('#CAREER_OTHER').val(),
        STATUS: checkeundefined($('input[name=STATUS]:checked').val()),
        VDRL: checkenull($('#VDRL').val()),
        PRE_WEIGHT_ANTENATAL_CARE: $('#PRE_WEIGHT_ANTENATAL_CARE').val(),
        HIGHT_2: $('#HIGHT_2').val(),
        HIGHT_3: $('#HIGHT_3').val(),
        BMI1: $('#BMI1').val(),
        BMI2: $('#BMI2').val(),
        BMI3: $('#BMI3').val(),
        OB_DEPARTMENT: $('#OB_DEPARTMENT').val(),
        POSITION: $('#POSITION').val(),
        SEND_DEPARTMENT: $('#SEND_DEPARTMENT').val(),
      }).then(function (response) {

        //   console.log(response.data)

      })
      .catch(function (error) {
        console.log(error)
      })
    M.toast({
      "html": 'บันทึกข้อมูลเรียบร้อย'
    })
    Router.go('result')
  }
})

function getprovince() {
  axios
    .post(`${Meteor.settings.public.api_host}/get-province`)
    .then(function (response) {
      console.log(response.data)
      Meteor.setTimeout(() => {
        $('select').formSelect()
      }, 500)
      Session.set('province', response.data)
    })
    .catch(function (error) {
      console.log(error)
    })
}

function getdistrict_by_province(province) {
  axios
    .post(`${Meteor.settings.public.api_host}/get-district`, {
      province: province
    })
    .then(function (response) {
      console.log(response.data)
      Meteor.setTimeout(() => {
        $('select').formSelect()
      }, 500)
      Session.set('district', response.data)
    })
    .catch(function (error) {
      console.log(error)
    })
}

function getsubdistrict_by_district(district, province) {
  axios
    .post(`${Meteor.settings.public.api_host}/get-subdistrict`, {
      district: district,
      province: province
    })
    .then(function (response) {
      console.log(response.data)
      Meteor.setTimeout(() => {
        $('select').formSelect()
      }, 500)
      Session.set('subdistrict', response.data)
    })
    .catch(function (error) {
      console.log(error)
    })
}

function gethospictal_by_subdistrict(subdistrict, district, province) {
  axios
    .post(`${Meteor.settings.public.api_host}/get-hospital`, {
      subdistrict: subdistrict,
      district: district,
      province: province
    })
    .then(function (response) {
      console.log(response.data)
      Meteor.setTimeout(() => {
        $('select').formSelect()
      }, 500)
      Session.set('hospital', response.data)
    })
    .catch(function (error) {
      console.log(error)
    })
}


function ninetynine() {
  var arr = []
  for (var index = 1; index < 100; index++) {
    arr.push({
      index: index
    })
  }
  Session.set('number', arr)
}

function calculateBmi(weight, height) {

  if (weight > 0 && height > 0) {
    var finalBmi = weight / (height / 100 * height / 100)

    if (finalBmi < 18.5) {
      return finalBmi.toFixed(1);
      // console.log("ผอม" + finalBmi)
    } else if (finalBmi > 18.5 && finalBmi < 23) {
      return finalBmi.toFixed(1);
      // console.log("ปกติ")
    } else if (finalBmi >= 23 && finalBmi < 25) {
      return finalBmi.toFixed();
      // console.log("น้ำหนักเกิน ")
    } else if (finalBmi >= 25.0 && finalBmi < 30) {
      return finalBmi.toFixed(1);
      // console.log("อ้วน")
    } else if (finalBmi >= 30) {
      return finalBmi.toFixed(1);
      // console.log("อ้วนมาก")
    }

  } else {
    console.log("...")
  }
}
function loginkad(){
  dataDomain = JSON.parse(localStorage.getItem('Domainlogin'))
  dataadmin = JSON.parse(localStorage.getItem('adminlogin'))
  if(dataadmin.RULE=='4'){
    //admin

  }else if(dataDomain.RULE=='6'){
    //domain
 $("#provence-div *").attr("disabled", "disabled")
 $("#district-div *").attr("disabled", "disabled")
  }else{
 
  }
 
}