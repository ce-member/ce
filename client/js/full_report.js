const axios = require('axios')

Template.full_report.onRendered(function () {
    
    axios
      .post(`${Meteor.settings.public.api_host}/get-ce`)
      .then(function (response) {
        Session.set('data', response.data[0])
        $('input[name="nation"]')[parseInt(Session.get('data').NATIONALITY)-1].checked = true;
        if(Session.get('data').WOMB_RISK == '1'){
            $('input[name="check3_1"]')[0].checked = true;
        }else{
            $('input[name="check3_1"]')[1].checked = true;
        }
        
        if(Session.get('data').WOMB_CH1 == '1'){
            $('input[name="check3_2_1"]')[0].checked = true;
        }
        if(Session.get('data').WOMB_CH2 == '1'){
            $('input[name="check3_2_2"]')[0].checked = true;
        }
        if(Session.get('data').WOMB_CH3 == '1'){
            $('input[name="check3_2_3"]')[0].checked = true;
        }
        if(Session.get('data').WOMB_CH4 == '1'){
            $('input[name="check3_2_4"]')[0].checked = true;
        }
        if(Session.get('data').WOMB_CH5 == '1'){
            $('input[name="check3_2_5"]')[0].checked = true;
        }
        if(Session.get('data').WOMB_CH6 == '1'){
            $('input[name="check3_2_6"]')[0].checked = true;
        }
        if(Session.get('data').WOMB_CH7 == '1'){
            $('input[name="check3_2_7"]')[0].checked = true;
        }
        
        if(Session.get('data').STATUS_COME_SERVICE_1 == '1'){
            $('input[name="check4_1_1"]')[0].checked = true;
        }
        if(Session.get('data').STATUS_COME_SERVICE_2 == '1'){
            $('input[name="check4_1_2"]')[0].checked = true;
        }
        if(Session.get('data').STATUS_COME_SERVICE_3 == '1'){
            $('input[name="check4_1_3"]')[0].checked = true;
        }
        if(Session.get('data').STATUS_COME_SERVICE_4 == '1'){
            $('input[name="check4_1_4"]')[0].checked = true;
        }
        if(Session.get('data').STATUS_COME_SERVICE_5 == '1'){
            $('input[name="check4_1_5"]')[0].checked = true;
        }
        if(Session.get('data').STATUS_COME_SERVICE_6 == '1'){
            $('input[name="check4_1_6"]')[0].checked = true;
        }

        if(Session.get('data').STATUS_PATIENT_AT_SERVICE_1 == '1'){
            $('input[name="check4_2_1"]')[0].checked = true;
        }
        if(Session.get('data').STATUS_PATIENT_AT_SERVICE_2 == '1'){
            $('input[name="check4_2_2"]')[0].checked = true;
        }
        if(Session.get('data').STATUS_PATIENT_AT_SERVICE_3 == '1'){
            $('input[name="check4_2_3"]')[0].checked = true;
        }
        if(Session.get('data').STATUS_PATIENT_AT_SERVICE_4 == '1'){
            $('input[name="check4_2_4"]')[0].checked = true;
        }
        
        if(Session.get('data').PARTOGRAPH == '1'){
            $('input[name="check5_1"]')[0].checked = true;
        }else{
            $('input[name="check5_1"]')[1].checked = true;
        }
        if(Session.get('data').MISCARRIAGE == '1'){
            $('input[name="check5_2"]')[0].checked = true;
        }else{
            $('input[name="check5_2"]')[1].checked = true;
        }
        if(Session.get('data').BIRTH){
            $('input[name="check5_3"]')[parseInt(Session.get('data').BIRTH)-1].checked = true;
        }
        if(Session.get('data').ACCOUCHEUR){
            $('input[name="check5_4"]')[parseInt(Session.get('data').ACCOUCHEUR)-1].checked = true;
        }

        if(Session.get('data').CHOICE1){
            $('input[name="check6_1"]')[parseInt(Session.get('data').CHOICE1)-1].checked = true;
        }
        if(Session.get('data').CHOICE2){
            $('input[name="check6_2"]')[parseInt(Session.get('data').CHOICE2)-1].checked = true;
        }
        if(Session.get('data').CHOICE3){
            $('input[name="check6_3"]')[parseInt(Session.get('data').CHOICE3)-1].checked = true;
        }
        if(Session.get('data').CHOICE4){
            $('input[name="check6_4"]')[parseInt(Session.get('data').CHOICE4)-1].checked = true;
        }
        if(Session.get('data').CHOICE5){
            $('input[name="check6_5"]')[parseInt(Session.get('data').CHOICE5)-1].checked = true;
        }
        if(Session.get('data').CHOICE6){
            $('input[name="check6_6"]')[parseInt(Session.get('data').CHOICE6)-1].checked = true;
        }
        if(Session.get('data').CHOICE7){
            $('input[name="check6_7"]')[parseInt(Session.get('data').CHOICE7)-1].checked = true;
        }
        if(Session.get('data').CHOICE8){
            $('input[name="check6_8"]')[parseInt(Session.get('data').CHOICE8)-1].checked = true;
        }
        if(Session.get('data').CHOICE9){
            $('input[name="check6_9"]')[parseInt(Session.get('data').CHOICE9)-1].checked = true;
        }

        if(Session.get('data').CH_1 == '1'){
            $('#CH_1_TRUE').css('display', 'block')
            $('#CH_1_FALSE').css('display', 'none')
        }else{
            $('#CH_1_TRUE').css('display', 'none')
            $('#CH_1_FALSE').css('display', 'block')
        }
        if(Session.get('data').SUB_CH_1 != ''){
            $('#SUB_CH_1').text(Session.get('data').SUB_CH_1)
        }else{
            $('#SUB_CH_1').text('')
        }

        if(Session.get('data').CH_1_2 == '1'){
            $('#CH_1_2_TRUE').css('display', 'block')
            $('#CH_1_2_FALSE').css('display', 'none')
        }else{
            $('#CH_1_2_TRUE').css('display', 'none')
            $('#CH_1_2_FALSE').css('display', 'block')
        }
        if(Session.get('data').SUB_CH_1_2 != ''){
            $('#SUB_CH_1_2').text(Session.get('data').SUB_CH_1_2)
        }else{
            $('#SUB_CH_1_2').text('')
        }

        if(Session.get('data').CH_2 == '1'){
            $('#CH_2_TRUE').css('display', 'block')
            $('#CH_2_FALSE').css('display', 'none')
        }else{
            $('#CH_2_TRUE').css('display', 'none')
            $('#CH_2_FALSE').css('display', 'block')
        }
        if(Session.get('data').SUB_CH_2 != ''){
            $('#SUB_CH_2').text(Session.get('data').SUB_CH_2)
        }else{
            $('#SUB_CH_2').text('')
        }

        if(Session.get('data').CH_2_2 == '1'){
            $('#CH_2_2_TRUE').css('display', 'block')
            $('#CH_2_2_FALSE').css('display', 'none')
        }else{
            $('#CH_2_2_TRUE').css('display', 'none')
            $('#CH_2_2_FALSE').css('display', 'block')
        }
        if(Session.get('data').SUB_CH_2_2 != ''){
            $('#SUB_CH_2_2').text(Session.get('data').SUB_CH_2_2)
        }else{
            $('#SUB_CH_2_2').text('')
        }

        if(Session.get('data').CH_2_3 == '1'){
            $('#CH_2_3_TRUE').css('display', 'block')
            $('#CH_2_3_FALSE').css('display', 'none')
        }else{
            $('#CH_2_3_TRUE').css('display', 'none')
            $('#CH_2_3_FALSE').css('display', 'block')
        }
        if(Session.get('data').SUB_CH_2_3 != ''){
            $('#SUB_CH_2_3').text(Session.get('data').SUB_CH_2_3)
        }else{
            $('#SUB_CH_2_3').text('')
        }

        if(Session.get('data').CH_3 == '1'){
            $('#CH_3_TRUE').css('display', 'block')
            $('#CH_3_FALSE').css('display', 'none')
        }else{
            $('#CH_3_TRUE').css('display', 'none')
            $('#CH_3_FALSE').css('display', 'block')
        }
        if(Session.get('data').SUB_CH_3 != ''){
            $('#SUB_CH_3').text(Session.get('data').SUB_CH_3)
        }else{
            $('#SUB_CH_3').text('')
        }

        if(Session.get('data').CH_3_2 == '1'){
            $('#CH_3_2_TRUE').css('display', 'block')
            $('#CH_3_2_FALSE').css('display', 'none')
        }else{
            $('#CH_3_2_TRUE').css('display', 'none')
            $('#CH_3_2_FALSE').css('display', 'block')
        }
        if(Session.get('data').SUB_CH_3_2 != ''){
            $('#SUB_CH_3_2').text(Session.get('data').SUB_CH_3_2)
        }else{
            $('#SUB_CH_3_2').text('')
        }

        if(Session.get('data').CH_3_3 == '1'){
            $('#CH_3_3_TRUE').css('display', 'block')
            $('#CH_3_3_FALSE').css('display', 'none')
        }else{
            $('#CH_3_3_TRUE').css('display', 'none')
            $('#CH_3_3_FALSE').css('display', 'block')
        }
        if(Session.get('data').SUB_CH_3_3 != ''){
            $('#SUB_CH_3_3').text(Session.get('data').SUB_CH_3_3)
        }else{
            $('#SUB_CH_3_3').text('')
        }

        if(Session.get('data').CH_3_4 == '1'){
            $('#CH_3_4_TRUE').css('display', 'block')
            $('#CH_3_4_FALSE').css('display', 'none')
        }else{
            $('#CH_3_4_TRUE').css('display', 'none')
            $('#CH_3_4_FALSE').css('display', 'block')
        }
        if(Session.get('data').SUB_CH_3_4 != ''){
            $('#SUB_CH_3_4').text(Session.get('data').SUB_CH_3_4)
        }else{
            $('#SUB_CH_3_4').text('')
        }

        if(Session.get('data').MISCARRIAGE == '1'){
            $('input[name="check8"]')[0].checked = true;
        }else{
            $('input[name="check8"]')[1].checked = true;
        }

        if(Session.get('data').PROTECT_DEAD == '1'){
            $('input[name="check10"]')[0].checked = true;
        }else{
            $('input[name="check10"]')[1].checked = true;
        }

      })
      .catch(function (error) {
        console.log(error)
      })
})

Template.full_report.helpers({
    getData(){
        if(Session.get('data')){
            return Session.get('data')
        }
    }
})

Template.registerHelper('getDay', function(x) {
    return moment(x).format('D');
});

Template.registerHelper('getMonth', function(x) {
    let data = moment(x).format('M');
    if(data == '1'){
        return 'มกราคม'
    }else if(data == '2'){
        return 'กุมภาพันธ์'
    }else if(data == '3'){
        return 'มีนาคม'
    }else if(data == '4'){
        return 'เมษายน'
    }else if(data == '5'){
        return 'พฤษภาคม'
    }else if(data == '6'){
        return 'มิถุนายน'
    }else if(data == '7'){
        return 'กรกฎาคม'
    }else if(data == '8'){
        return 'สิงหาคม'
    }else if(data == '9'){
        return 'กันยายน'
    }else if(data == '10'){
        return 'ตุลาคม'
    }else if(data == '11'){
        return 'พฤศจิกายน'
    }else if(data == '12'){
        return 'ธันวาคม'
    }
});

Template.registerHelper('getYear', function(x) {
    return parseInt(moment(x).format('YYYY'))+543;
});