Router.route('/c', function() {
    this.render('c');
});

Router.route('/ce', function() {
    this.render('ce');
});

Router.route('/easy_report', function() {
    this.render('easy_report');
});

Router.route('/full_report', function() {
    this.render('full_report');
});
Router.route('/result', function() {
    this.render('result');
});
Router.route('/c_view', function() {
    this.render('c_view');
});
Router.route('/ce_view', function() {
    this.render('ce_view');
});